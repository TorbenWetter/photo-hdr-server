import os
from flask import Flask, request
from waitress import serve
import _thread
import imutils
import cv2
import numpy as np
from dotenv import load_dotenv
import twitter

photos_dir = 'photos'

app = Flask(__name__)


def process(upload_urls, date_string, minutes_diff):
    # Download the images and load them as cv2 images
    images = [imutils.url_to_image(upload_url) for upload_url in upload_urls]

    # Merge the images with different shutter speeds (https://en.wikipedia.org/wiki/High_dynamic_range)
    response_mertens = cv2.createMergeMertens().process(images)
    response_mertens_8bit = np.clip(response_mertens * 255, 0, 255).astype('uint8')

    # Save the image
    photo_path = photos_dir + '/' + date_string + '.jpg'
    cv2.imwrite(photo_path, response_mertens_8bit)

    # Post tweet at time of sunset
    if minutes_diff == 0:
        api.PostUpdate('Sonnenuntergang in Mainz am ' + date_string.replace('_', ' '), media=photo_path,
                       latitude=50.019450, longitude=8.212241, display_coordinates=True)


@app.route('/', methods=['POST'])
def index():
    try:
        # Get information from request
        data = request.json
        upload_urls, date_string, minutes_diff = data['uploadUrls'], data['dateString'], data['minutesDiff']

        # Call process function asynchronously
        _thread.start_new_thread(process, (upload_urls, date_string, minutes_diff,))
    except Exception as error:
        return {'success': False, 'error': error}, 418
    return {'success': True}, 200


if __name__ == '__main__':
    load_dotenv()

    if not os.path.exists(photos_dir):
        os.makedirs(photos_dir)

    api = twitter.Api(consumer_key=os.getenv("TWITTER_CONSUMER_KEY"),
                      consumer_secret=os.getenv("TWITTER_CONSUMER_SECRET"),
                      access_token_key=os.getenv("TWITTER_ACCESS_TOKEN_KEY"),
                      access_token_secret=os.getenv("TWITTER_ACCESS_TOKEN_SECRET"))

    serve(app, host="127.0.0.1", port=29124)
